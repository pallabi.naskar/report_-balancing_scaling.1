# Load balancers and vertical/horizontal scaling 

## **Introduction:**
In computing, load balancing refers to the process of distributing a set of 
 tasks over a set of resources (computing units), 
 with the aim of making their overall processing more efficient. 
 Load balancing is the subject of research in the field of parallel computers.
 ###  Hardware architecture of Load Balancing:
 * Heterogenous machines
 * Shared and distributed memory
 * Hierarchy
 * Adaptation to larger architectures (scalability)
 * Fault tolerance
 ## Relation between load balancing and scalability:
An extremely important parameter of a load balancing algorithm is therefore 
its ability to adapt to a scalable hardware architecture. This is called the scalability of the algorithm. 
An algorithm is called scalable for an input parameter when its 
performance remains relatively independent of the size of that parameter.
## Definition of scalability and type of scalability:
Scalability is the property of a system to handle a growing amount of work by adding resources to the system.
scalability are two type:
1. Horizontal or Scale Out
2. Vertical or Scale Up
## Horizontal or Scale Out and Vertical or Scale Up:
Scalability is being able to handle more request and both vertical and horizontal scaling 
handle these request differently.
Horizontal scaling means that you scale by adding more machines into your pool of resources
whereas Vertical scaling means that you scale by adding 
more power (CPU, RAM) to an existing machine.


![system schema](https://secureservercdn.net/160.153.138.71/a56.84d.myftpupload.com/wp-content/uploads/2019/04/Scaling.png)


A database world horizontal-scaling is often based on the partitioning of the data i.e.
 each node contains only part of the data, in vertical-scaling the data resides on a single node and scaling 
is done through multi-core i.e. spreading the load between the CPU and RAM resources of that machine.

With horizontal-scaling it is often easier to scale dynamically by adding more machines
 into the existing pool - Vertical-scaling is often limited to the capacity of a single machine,
  scaling beyond that capacity often involves downtime and comes with an upper limit.

  * Good examples of horizontal scaling are Cassandra, MongoDB, 
  * Good example of vertical scaling is MySQL

  ## conclusion:
  Horizontal scaling is almost always more desirable than vertical scaling because
 you don’t get caught in a resource deficit. Instead of taking your server offline
while you’re scaling up to a better one, horizontal scaling lets you keep your existing pool
 of computing resources online while adding more to what you already have. When your app is scaled horizontally, 
you have the benefit of elasticity.


### reference:
https://en.wikipedia.org/wiki/Load_balancing_(computing)

https://en.wikipedia.org/wiki/Scalability

https://dev.to/wmahathre/horizontal-and-vertical-scaling-1lid#:~:text=Horizontal%20scaling%20has%20what's%20called,to%20send%20requests%20to%20it.

https://medium.com/@abhinavkorpal/scaling-horizontally-and-vertically-for-databases-a2aef778610c



